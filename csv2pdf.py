from fpdf import FPDF

title = "Parsing CSV to PDF"


class PDF(FPDF):
    def header(self):
        self.set_font('Arial', 'B', 15)
        w = self.get_string_width(title) + 6
        self.set_x((210 - w) / 2)
        self.set_draw_color(190, 190, 190)
        self.set_fill_color(125, 125, 0)
        self.set_text_color(225, 255, 255)
        self.set_line_width(0.6)
        self.cell(w, 9, title, 1, 1, 'C', 1)
        self.ln(10)

    def footer(self):
        self.set_y(-10)
        self.set_font('Arial', 'I', 6)
        self.cell(0, 8, 'Page '+str(self.page_no())+'/{nb}', 0, 0, 'C')

    def chapter_title(self, filename):
        self.set_font('Arial', 'B', 12)
        self.set_fill_color(125, 190, 0)
        with open(filename, 'r') as file:
            line = file.readline()
            self.cell(0, 15, line, 0, 2)

    def chapter_body(self, filename):
        self.set_font('Arial', '', 10)
        with open(filename, 'r') as file:
            next(file)
            for line in file.readlines():
                self.cell(0, 12, line, 0, 2)

    def print_page(self, title, filename):
        self.add_page()
        self.chapter_title(filename)
        self.chapter_body(filename)
        self.output(
            'Documents/aaroon/Python Assignments/Asgnt_4_2_/csv2pdf.pdf', 'F')


filename = 'Documents/aaroon/Python Assignments/Asgnt_4_2_/biostats.csv'
pdf = PDF()
pdf.alias_nb_pages()
pdf.print_page(title, filename)
